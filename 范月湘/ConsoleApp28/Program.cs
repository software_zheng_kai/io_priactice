﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp28
{
    class Program
    {
        static void Main(string[] args)
        {
            DriveInfo driveInfo = new DriveInfo("D");
            Console.WriteLine("驱动器的名称:" + driveInfo.Name);
            Console.WriteLine("驱动器类型:" + driveInfo.DriveType);
            Console.WriteLine("驱动器的文件格式:" + driveInfo.DriveFormat);
            Console.WriteLine("驱动器中可用文件大小:" + driveInfo.TotalFreeSpace);
            Console.WriteLine("驱动器总大小:" + driveInfo.TotalSize);

            DriveInfo[] driveInfos = DriveInfo.GetDrives();
            foreach (DriveInfo info in driveInfos)
            {
                if (info.IsReady)
               {
                    Console.WriteLine("驱动器名称:" + info.Name);
                   Console.WriteLine("驱动器的文件格式:" + info.DriveFormat);
               }
            }
            //在D盘创建文件夹mySolo，并在该文件夹中创建test.txt子文件夹
            DirectoryInfo directoryInfo = new DirectoryInfo("D:\\test.txt");
            directoryInfo.Create();
            directoryInfo.CreateSubdirectory("test.txt");
            //查看D盘下mySolo文件夹中的文件夹
            IEnumerable<DirectoryInfo> dir = directoryInfo.EnumerateDirectories();
            foreach (var v in dir)
            {
                Console.WriteLine(v.Name);
            }
            //将mySolo文件夹及其含有的子文件夹删除
            //DirectoryInfo directoryInfo = new DirectoryInfo("D:\\mySolo");
            //directoryInfo.Delete(true);

            //判断是否存在该文件夹
            bool moon = Directory.Exists("D:\\mySolo");
            if (moon)
            {
                Directory.Delete("D:\\mySolo", true);
            }
            else
            {
                Directory.CreateDirectory("D:\\mySolo");
            }

            FileInfo fileInfo = new FileInfo("D:\\mySolo\\test.txt");
            fileInfo.Create();
            string path = @"D:\\mySolo\\test.txt";
            StreamWriter streamWrite = new StreamWriter(path);
            streamWrite.WriteLine("烟雨行舟");
            streamWrite.WriteLine("长安姑娘");
            streamWrite.WriteLine("盗墓笔记·十年人间");
            streamWrite.WriteLine("一抹桃花");
            streamWrite.WriteLine("狐言");
            streamWrite.Flush();
            streamWrite.Close();
        }
    }
}
