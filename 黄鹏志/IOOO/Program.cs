﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IOOO
{
    class Program
    {
        static void Main(string[] args)
        {

            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (DriveInfo d in drives)
            {
                if (d.IsReady)
                {
                    Console.WriteLine("总容量：" + d.TotalFreeSpace);
                    Console.WriteLine("可用容量：" + d.AvailableFreeSpace);
                    Console.WriteLine("驱动器类型：" + d.DriveFormat);
                    Console.WriteLine("驱动器的名称：" + d.Name + "\n");
                }
            }
            Console.WriteLine("OK!");
            Console.ReadLine();




            //添加文件夹
            FileInfo file = new FileInfo(@"F:\\git工作 \\test.txt"); // 创建文件
            Console.WriteLine(" 创建时间： " + file.CreationTime);
            Console.WriteLine(" 路径： " + file.DirectoryName);
            StreamWriter sw = file.AppendText(); // 打开追加流
            sw.Write(" 小波 "); // 追加数据
            sw.Dispose(); // 释放资源 , 关闭文件
            Console.WriteLine(" 完成！ ");
            Console.ReadLine();

            //FileStream fileStream = new FileStream(@"D:\\code\\test.txt", FileMode.Open, FileAccess.Write);
            //创建二进制写入流的实例
            //BinaryWriter binaryWriter = new BinaryWriter(fileStream);
            //向文件中写入图书名称
            //binaryWriter.Write("C#基础教程");
            //向文件中写入图书价格
            //binaryWriter.Write(49.5);
            //清除缓冲区的内容，将缓冲区中的内容写入到文件中
            //binaryWriter.Flush();
            //关闭二进制流
            //binaryWriter.Close();
            //关闭文件流
            //fileStream.Close();
            //fileStream = new FileStream(@"D:\\code\\test.txt", FileMode.Open, FileAccess.Read);
            //创建二进制读取流的实例
            //BinaryReader binaryReader = new BinaryReader(fileStream);
            //输出图书名称
            //Console.WriteLine(binaryReader.ReadString());
            //输出图书价格
            //Console.WriteLine(binaryReader.ReadDouble());
            //关闭二进制读取流
            //binaryReader.Close();
            //关闭文件流
            //fileStream.Close();
        }
    }
}
