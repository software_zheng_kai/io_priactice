﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoPractic
{
    class Program
    {
        static void Main(string[] args)
        {
            //获取 D 盘中的驱动器类型、名称、文件系统名称、可用空间以及总空间大小

            DriveInfo driveInfo = new DriveInfo("D:");
            Console.WriteLine(driveInfo.Name + "盘");
            Console.WriteLine("驱动器类型：" + driveInfo.DriveType);
            Console.WriteLine("可用空间为：" + driveInfo.TotalFreeSpace);
            Console.WriteLine("文件格式：" + driveInfo.DriveFormat);
            Console.WriteLine("驱动器总大小：" + driveInfo.TotalSize);

            //获取计算机中所有驱动器的名称和文件格式。
            Console.WriteLine();
            DriveInfo[] driveInfo2 = DriveInfo.GetDrives();
            foreach (DriveInfo item in driveInfo2)
            {
                if (item.IsReady)
                {
                    Console.WriteLine("驱动器名称：" + item.Name);
                    Console.WriteLine("驱动器的文件格式" + item.DriveFormat);
                }
            }

            Console.WriteLine();
            //在 D 盘下创建文件夹 code，并在该文件夹中创建 code-1和 code-2 两个子文件夹。
            DirectoryInfo directoryInfo = new DirectoryInfo("D:\\Code");

            directoryInfo.Create();
            directoryInfo.CreateSubdirectory("Code-1");
            directoryInfo.CreateSubdirectory("Code-2");

            //判断文件夹Code 中是不是创建成功
            bool flag = Directory.Exists("D:\\Code");
            if (flag)
            {
                Console.WriteLine("文件夹生成成功\n");
            }
            else
            {
                Console.WriteLine("文件夹生成失败，我来帮你建一个");
                Directory.CreateDirectory("D:\\Code");
            }

            //查看文件夹中的文件夹
            IEnumerable<DirectoryInfo> dir = directoryInfo.EnumerateDirectories();
            foreach(var v in dir)
            {
                Console.WriteLine(v.Name);
            }

            //向文件夹中添加文件
            FileInfo fileInfo = new FileInfo("D:\\Code\\Test1.txt");
            if (!fileInfo.Exists)
            {
                fileInfo.Create().Close();//创建文件
            }
            else
            {
                Console.WriteLine("文件创建失败，此位置已包含同名文件");
            }
            //判断是否生成成功文件
            if (fileInfo.Exists)
            {
                Console.WriteLine("\n"+fileInfo+"文件存在");
            }
            else
            {
                Console.WriteLine("\n文件创建失败");
            }
            Console.WriteLine();

            fileInfo.Attributes = FileAttributes.Normal;//设置文件属性

            Console.WriteLine("文件路径：" + fileInfo.Directory);
            Console.WriteLine("文件名称：" + fileInfo.Name);
            Console.WriteLine("文件是否只读：" + fileInfo.IsReadOnly);
            Console.WriteLine("文件大小：" + fileInfo.Length);
            Console.WriteLine();

            //移动文件到指定路径
            FileInfo fileInfo1 = new FileInfo("D:\\Code-1\\Test1.txt");
            if (!fileInfo1.Exists)
            {
                fileInfo.MoveTo("D:\\Code\\Code-1\\Test1.txt");//中国移动我得动
            }
            
            //是否移动成功
            if (!fileInfo.Exists)
            {
                Console.WriteLine("移动失败");
            }
            else
            {
                Console.WriteLine("移动成功");
            }


            Console.WriteLine();
            Console.WriteLine("是否删除删除文件夹 删除请打1");
            int delecFolder =int.Parse(Console.ReadLine());
            if (delecFolder == 1)
            {
                //删除文件夹Code
                directoryInfo.Delete(true);
                bool delect = Directory.Exists("D:\\Code");
                if (delect)
                {
                    Console.WriteLine("\n删除失败");
                }
                else
                {
                    Console.WriteLine("\n删除成功");
                }
            }
            else
            {
                //删除文件夹Code
                directoryInfo.Delete(true);
                bool delect = Directory.Exists("D:\\Code");
                if (delect)
                {
                    Console.WriteLine("\n删除失败");
                }
                else
                {
                    Console.WriteLine("\n删除成功");
                }
                Console.WriteLine("\n不删再运行就会报错 所以我还是删了 \n");//由于Code-1中的文件未删除所以再运行时会报错
            }


            Console.WriteLine();



        }
    }
}
