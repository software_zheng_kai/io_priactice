# 文件IO练习

#### 介绍

1. 在D盘根目录下创建一个叫：mySolo的文件夹（注意，创建前可能需要判断该文件夹是否存在）；
2. 在上一步创建的目录中，创建一个叫test.txt的文件；
3. 在第2步创建的文件中，写内容，比如最喜欢的几首歌等；
（注意，以上任务，可不限定于Driveinfo 类、Directory 类、Directoryinfo 类、File 类、Filelnfo 类、Path 类等，可自由选择和组合）
