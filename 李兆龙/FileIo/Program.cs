﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FileIo
{
    class Program
    {
        static void Main(string[] args)
        {
            //*****获取MyComputer基本信息*****
            DriveInfo[] allDrives = DriveInfo.GetDrives();

            foreach (DriveInfo item in allDrives)
            {
                Console.WriteLine("Drive :{0}", item.Name);
                Console.WriteLine("Drive Type :{0}", item.DriveType);
                Console.WriteLine("File System :{0}", item.DriveFormat);//文件属性
                Console.WriteLine("Root Directory(根目录){0} :", item.RootDirectory);
                Console.WriteLine("VolumeLabel:{0}", item.VolumeLabel);//VolumeLabel属性， 获取或设置驱动器的卷标
                Console.WriteLine("Total Size :{0}", item.TotalSize);
                if (item.IsReady == true)
                {
                    Console.WriteLine("Total available space:          {0, 15} bytes ", item.TotalFreeSpace);
                    Console.WriteLine("Available space to current user:{0, 15} bytes", item.AvailableFreeSpace);
                }
            }

            //*****Directoryinfo类：文件夹操作*****
            //在D盘创建文件夹code并且创建文件夹-1与-2

            DirectoryInfo directoryInfo = new DirectoryInfo("D:\\code");
            directoryInfo.Create();
            directoryInfo.CreateSubdirectory("code-1");
            directoryInfo.CreateSubdirectory("code-2");

            //查看D盘下code文件夹中的文件夹
            IEnumerable<DirectoryInfo> dir = directoryInfo.EnumerateDirectories();//EnumerateDirectories 方法只用于检索文件夹，不能检索文件
            foreach (var i in dir)
            {
                Console.WriteLine(i.Name);
            }

            //删除文件夹code
            directoryInfo.Delete(true);

            //*****Directory类：文件夹操作*****
            //使用 Directory 类在 D 盘上操作 File 文件夹，要求先判断是否存在该文件夹，如果存在则删除，否则创建该文件夹

            bool aph = Directory.Exists("D:\\File");
            if (aph)
            {
                Directory.Delete("D:\\File", true);
            }
            else
            {
                Directory.CreateDirectory("D:\\File");
            }

            //*****FileInfo类：文件操作*****
            //在 D 盘的 Test 文件夹下创建名为 test1.txt 的文件，并获取该文件的相关属性，然后将其移动到D盘下的 test-1 文件夹中
            Directory.CreateDirectory("D:\\Test");
            FileInfo fileInfo = new FileInfo("D:\\Test\\test1.txt");
            if (!fileInfo.Exists)
            {
                //创建文件
                fileInfo.Create().Close();
            }
            fileInfo.Attributes = FileAttributes.Normal;//设置文件属性
            Console.WriteLine("文件路径：" + fileInfo.Directory);
            Console.WriteLine("文件名称：" + fileInfo.Name);
            Console.WriteLine("文件是否只读：" + fileInfo.IsReadOnly);
            Console.WriteLine("文件大小：" + fileInfo.Length);
            //先创建Test-1 文件夹
            //将文件移动到Test-1文件夹下
            Directory.CreateDirectory("D:\\Test-1");
            //判断目标文件夹中是否含有文件test1.txt
            FileInfo newFileInfo = new FileInfo("D:\\Test-1\\test1.txt");
            if (!newFileInfo.Exists)
            {
                //移动文件到指定路径
                fileInfo.MoveTo("D:\\Test-1\\test1.txt");
            }

            //*****File类：文件操作*****
            //将上一节《C# FileInfo》实例中实现的内容使用 File 类完成
            //在D盘下创建code文件夹
            Directory.CreateDirectory("D:\\Test");
            Directory.CreateDirectory("D:\\Test-1");
            string path = "D:\\Test\\test1.txt";
            //创建文件
            FileStream fs = File.Create(path);
            //获取文件信息
            Console.WriteLine("文件创建时间：" + File.GetCreationTime(path));
            Console.WriteLine("文件最后被写入时间：" + File.GetLastWriteTime(path));
            //关闭文件流
            fs.Close();
            //设置目标路径
            string newPath = "D:\\Test-1\\test1.txt";
            //判断目标文件是否存在
            bool flag = File.Exists(newPath);
            if (flag)
            {
                //删除文件
                File.Delete(newPath);
            }
            File.Move(path, newPath);

            //*****Path类：文件路径操作*****
            //从控制台输入一个路径，输出该路径的不含扩展名的路径、扩展名、文件全 名、文件路径、更改文件扩展名
            Console.WriteLine("**********请输入一个文件路径**********");
            string paths = Console.ReadLine();
            Console.WriteLine("不包含扩展名的文件名：" + Path.GetFileNameWithoutExtension(paths));
            Console.WriteLine("文件扩展名：" + Path.GetExtension(paths));
            Console.WriteLine("文件全名：" + Path.GetFileName(paths));
            Console.WriteLine("文件路径："+Path.GetDirectoryName(paths));
            //更改文件扩展名
            string newPaths = Path.ChangeExtension(paths, "doc");
            Console.WriteLine("更改后的文件全名为："+Path.GetFileName(paths));

        }
    }
}
