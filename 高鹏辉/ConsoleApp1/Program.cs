﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream F = new FileStream("test.dat",FileMode.OpenOrCreate,FileAccess.ReadWrite);
            for (int i = 0; i<=10;i++)
            {
                F.WriteByte((byte)i);
            }
            F.Position = 0;
            for (int i =0;i<=10;i++)
            {
                Console.Write(F.ReadByte()+" ");
            }   
            F.Close();
            DirectoryInfo D = new DirectoryInfo("D:\\shuai");
            D.Create();
            D.CreateSubdirectory("shuai+1");
            D.CreateSubdirectory("shuai+2");
            IEnumerable<DirectoryInfo> ts = D.EnumerateDirectories();
            foreach(var a in ts)
            {
                Console.Write(a.Name+" ");
            }
            D.Delete(true);
            bool chak = Directory.Exists("D:\\shuai");
            if (chak)
            {
                Directory.Delete("D:\\shuai",true);
            }
            else
            {
                Directory.CreateDirectory("D:\\shuai");
                D.CreateSubdirectory("shuai+1");
                D.CreateSubdirectory("shuai+2");
            }
            Directory.CreateDirectory("D:\\shuai");
            FileInfo W = new FileInfo("D:\\shuai\\test1.txt");
            if (!W.Exists)
            {
                W.Create().Close();
            }
            W.Attributes = FileAttributes.Normal;
            Console.WriteLine("文件路径：" + W.Directory);
            Console.WriteLine("文件名称：" + W.Name);
            Console.WriteLine("文件是否只读：" + W.IsReadOnly);
            Console.WriteLine("文件大小：" + W.Length);
            Directory.CreateDirectory("D:\\code-1");
            FileInfo newFileInfo = new FileInfo("D:\\code-1\\test1.txt");
            if (!newFileInfo.Exists)
            {
                W.MoveTo("D:\\shuai+1\\test1.txt");
            }
            Directory.CreateDirectory("D:\\shuai");
            Directory.CreateDirectory("D:\\shuai+1");
            string path = "D:\\shuai\\test1.txt";
            FileStream fs = File.Create(path);
            Console.WriteLine("文件创建时间：" + File.GetCreationTime(path));
            Console.WriteLine("文件最后被写入时间：" + File.GetLastWriteTime(path));
            fs.Close();
            string newPath = "D:\\code-1\\test1.txt";
            
            bool flag = File.Exists(newPath);
            if (flag)
            {
                File.Delete(newPath);
            }
            File.Move(path, newPath);
        }
    }
    }
}
