﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            //文件夹路径
            string directoryPath = "D:\\mySolo";

            //文件路径
            string filePath = "D:\\mySolo\\text.txt";

            //判断是否存在该文件夹
            bool flag = Directory.Exists(directoryPath);

            if (flag)
            {
                //删除文件夹
                Directory.Delete(directoryPath,true);
            }
            else
            {
                //创建文件夹
                Directory.CreateDirectory(directoryPath);
            }

            //创建文件
            FileStream file = File.Create(filePath);

            //写入文件
            StreamWriter writer = new StreamWriter(file);

            writer.WriteLine("时间不在于你拥有多少，而在于你怎样使用");
            
            //关闭流
            writer.Close();

            //关闭流（文件流最后关）
            file.Close();

            

        }
    }
}
