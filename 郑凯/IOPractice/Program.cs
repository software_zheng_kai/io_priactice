﻿

using System;
using System.Collections.Generic;
using System.IO;

namespace IOPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            DriveInfo driveInfo = new DriveInfo("D");
            Console.WriteLine("驱动器的名称：" + driveInfo.Name);
            Console.WriteLine("驱动器类型：" + driveInfo.DriveType);
            Console.WriteLine("驱动器的文件格式：" + driveInfo.DriveFormat);
            Console.WriteLine("驱动器中可用空间大小：" + driveInfo.TotalFreeSpace);
            Console.WriteLine("驱动器总大小：" + driveInfo.TotalSize);
            Console.WriteLine("练习一");
            Console.WriteLine();
            DriveInfo[] text = DriveInfo.GetDrives();
            foreach (DriveInfo d in text)
            {
                if (d.IsReady)
                {
                    Console.WriteLine("驱动器名称：" + d.Name);
                    Console.WriteLine("驱动器的文件格式" + d.DriveFormat);
                }
            }
            Console.WriteLine("练习二");
            Console.WriteLine();

            DirectoryInfo directoryInfo1 = new DirectoryInfo("D:\\code");
            
            directoryInfo1.Create();
            directoryInfo1.CreateSubdirectory("code-1");
            directoryInfo1.CreateSubdirectory("code-2");

            IEnumerable<DirectoryInfo> dir = directoryInfo1.EnumerateDirectories();
            foreach (var v in dir)
            {
                Console.WriteLine(v.Name);
            }

            directoryInfo1.Delete(recursive: true);
            Console.WriteLine("练习三");
            Console.WriteLine();

            bool flag = Directory.Exists("D:\\code");
            if (flag)
            {
                Directory.Delete("D:\\code", true);
                Console.WriteLine("删除成功");
            }
            else
            {
                Directory.CreateDirectory("D:\\code");
                Console.WriteLine("目录中未包含所选项目，已为你重新创建");
            }
            Console.WriteLine(  "练习四");
            Console.WriteLine();
        }
    }
}
