﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IODemo
{
    class Program
    {
        static void Main(string[] args)
        {
            DriveInfo driveInfo = new DriveInfo("D");
            Console.WriteLine("驱动器的名称：" + driveInfo.Name);
            Console.WriteLine("驱动器类型：" + driveInfo.DriveType);
            Console.WriteLine("驱动器的文件格式：" + driveInfo.DriveFormat);
            Console.WriteLine("驱动器中可用空间大小：" + driveInfo.TotalFreeSpace);
            Console.WriteLine("驱动器总大小：" + driveInfo.TotalSize);

            DriveInfo[] driveInfos = DriveInfo.GetDrives();
            foreach (DriveInfo info in driveInfos)
            {
                if (info.IsReady)
                {
                    Console.WriteLine("驱动器名称:" + info.Name);
                    Console.WriteLine("驱动器的文件格式:" + info.DriveFormat);
                }
            }
            {
                //在D盘下创建code文件夹
                Directory.CreateDirectory("D:\\code");
                FileInfo fileInfo = new FileInfo("D:\\code\\test1.txt");
                if (!fileInfo.Exists)
                {
                    //创建文件
                    fileInfo.Create().Close();
                }
                fileInfo.Attributes = FileAttributes.Normal;//设置文件属性
                Console.WriteLine("文件路径：" + fileInfo.Directory);
                Console.WriteLine("文件名称：" + fileInfo.Name);
                Console.WriteLine("文件是否只读：" + fileInfo.IsReadOnly);
                Console.WriteLine("文件大小：" + fileInfo.Length);
                //先创建code-1 文件夹
                //将文件移动到code-1文件夹下
                Directory.CreateDirectory("D:\\code-1");
                //判断目标文件夹中是否含有文件test1.txt
                FileInfo newFileInfo = new FileInfo("D:\\code-1\\test1.txt");
                if (!newFileInfo.Exists)
                {
                    //移动文件到指定路径
                    fileInfo.MoveTo("D:\\code-1\\test1.txt");
                }
            }
            {
                //在D盘下创建code文件夹
                Directory.CreateDirectory("D:\\code");
                Directory.CreateDirectory("D:\\code-1");
                string path = "D:\\code\\test1.txt";
                //创建文件
                FileStream fs = File.Create(path);
                //获取文件信息
                Console.WriteLine("文件创建时间：" + File.GetCreationTime(path));
                Console.WriteLine("文件最后被写入时间：" + File.GetLastWriteTime(path));
                //关闭文件流
                fs.Close();
                //设置目标路径
                string newPath = "D:\\code-1\\test1.txt";
                //判断目标文件是否存在
                bool flag = File.Exists(newPath);
                if (flag)
                {
                    //删除文件
                    File.Delete(newPath);
                }
                File.Move(path, newPath);
            }
            Console.WriteLine("请输入一个文件路径：");
            string path = Console.ReadLine();
            Console.WriteLine("不包含扩展名的文件名：" + Path.GetFileNameWithoutExtension(path));
            Console.WriteLine("文件扩展名：" + Path.GetExtension(path));
            Console.WriteLine("文件全名：" + Path.GetFileName(path));
            Console.WriteLine("文件路径：" + Path.GetDirectoryName(path));
            //更改文件扩展名
            string newPath = Path.ChangeExtension(path, "doc");
            Console.WriteLine("更改后的文件全名：" + Path.GetFileName(newPath));
        }
        
    }
}
