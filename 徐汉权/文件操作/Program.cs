﻿using System;
using System.Collections.Generic;
using System.IO;

namespace 文件操作
{
    class Program
    {
        static void Main(string[] args)
        {
            //// Driveinfo drive = new Driveinfo("D");

            ////Directoryinfo常用方法
            ////增、删、改、查。

            ////在D盘中创建文件夹
            //DirectoryInfo directoryInfo = new DirectoryInfo("D:\\Shqc");
            //directoryInfo.Create();
            ////在该文件夹中创建子文件夹
            //directoryInfo.CreateSubdirectory("Shqc-1");
            //directoryInfo.CreateSubdirectory("Shqc-2");
            ////查看D盘下Shqc文件夹中的文件
            //DirectoryInfo directoryInfo1 = new DirectoryInfo("D:\\Shqc");
            //IEnumerable<DirectoryInfo> dir = directoryInfo1.EnumerateDirectories();
            //foreach (var v in dir)
            //{
            //    Console.WriteLine(v.Name);
            //}
            ////删除D:\Shqc\Shqc-1
            ////需要注意的是，如果要删除一个非空文件夹，则要使用 Delete(True) 方法将文件夹中的文件一并删除，否则会岀现“文件夹不为空”的异常。
            ////delete
            //DirectoryInfo directoryInfo2 = new DirectoryInfo("D:\\Shqc\\Shqc-1");
            //directoryInfo2.Delete(true);
            ////-------------------------------------------------------------------------------------------------------------------------------
            ////-------------------------------------------------------------------------------------------------------------------------------
            ////-------------------------------------------------------------------------------------------------------------------------------
            ////Directory文件操作
            ////在D盘下查找Shqc如果有则删除
            ////没有则创建 
            //bool flag = Directory.Exists("D:\\Shqc");
            //if (flag)
            //{
            //    Directory.Delete("D:\\Shqc", true);
            //}
            //else {
            //    Directory.CreateDirectory("D:\\Shqc");
            //}
            ////-------------------------------------------------------------------------------------------------------------------------------
            ////-------------------------------------------------------------------------------------------------------------------------------
            ////-------------------------------------------------------------------------------------------------------------------------------
            ////FileInfo常用方法
            ////查询：文件路径。文件名称，文件是否只读，文件大小
            //Directory.CreateDirectory("D:\\WenAn");
            //FileInfo fileInfo = new FileInfo("D:\\WenAn\\wenan.txt");
            //if (!fileInfo.Exists)
            //{
            //    //创建文件
            //    fileInfo.Create().Close();
            //}
            ////设置文件属性
            //fileInfo.Attributes = FileAttributes.Normal;
            ////文件路径
            //Console.WriteLine("文件路径:"+fileInfo.Directory);
            ////文件名称
            //Console.WriteLine("文件名称"+fileInfo.Name);
            ////文件是否只读
            //Console.WriteLine("文件是否只读:"+fileInfo.IsReadOnly);
            ////文件大小
            //Console.WriteLine("文件大小:"+fileInfo.Length);
            //-------------------------------------------------------------------------------------------------------------------------------
            //-------------------------------------------------------------------------------------------------------------------------------
            //-------------------------------------------------------------------------------------------------------------------------------
            //File文件操作
            //
            Directory.CreateDirectory("D:\\WenAn\\wenan");
            Directory.CreateDirectory("D:\\WenAn\\wenan\\wenan1");
            string path = "D:\\WenAn\\wenan\\wenan1//wenan1.txt";
            //创建文件
            FileStream fs = File.Create(path);
            //获取文件信息
            Console.WriteLine("文件创建时间:"+File.GetCreationTime(path));
            Console.WriteLine("文件最后被写入时间:"+File.GetLastWriteTime(path));
            //关闭文件流
            fs.Close();
            //判断文件是否存在
            string bgc = "D:\\WenAn";
            bool lgae = File.Exists(bgc);
            if (lgae)
            {
                File.Delete(bgc);
            }
            File.Move(path, bgc);
        }
    }
}
