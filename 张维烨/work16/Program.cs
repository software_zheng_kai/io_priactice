﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work16
{
    class Program
    {
        static void Main(string[] args)
        {
            ////方法一：
            //DirectoryInfo directoryInfo = new DirectoryInfo("D:\\mySolo");//在D盘创建一个mySolo文件夹

            //directoryInfo.Delete(true);//先检查D盘有没有mySolo文件夹，如果有就删除。

            //directoryInfo.Create();//创建文件夹操作

            //FileInfo fileInfo = new FileInfo("D:\\mySolo\\test.txt");//在D盘的mySolo文件夹里创建一个test.txt的文件。

            //fileInfo.Create().Close();//创建文件操作

            //string path = @"D:\\mySolo\\test.txt";

            ////创建StreamWriter 类的实例
            //StreamWriter streamWriter = new StreamWriter(path);

            ////向文件中写入内容
            //streamWriter.WriteLine("老王爱洗澡，干净又卫生！");

            ////刷新缓存
            //streamWriter.Flush();

            ////关闭流
            //streamWriter.Close();

            //Console.WriteLine("操作成功");

            //方法二：       
            bool flag = Directory.Exists("D:\\mySolo");//判断是否已经存在该文件

            if (flag)
            {
                Directory.Delete("D:\\mySolo", true);
            }

            Directory.CreateDirectory("D:\\mySolo");//创建文件夹操作

            FileInfo fileInfo1 = new FileInfo("D:\\mySolo\\test.txt");//在D盘的mySolo文件夹里创建一个test.txt的文件。

            fileInfo1.Create().Close();//创建文件操作

            string path1 = @"D:\\mySolo\\test.txt";

            //创建StreamWriter 类的实例
            StreamWriter streamWriter1 = new StreamWriter(path1);

            //向文件中写入内容
            streamWriter1.WriteLine("老王爱洗澡，干净又卫生！!");

            //刷新缓存
            streamWriter1.Flush();

            //关闭流
            streamWriter1.Close();

            Console.WriteLine("操作成功");

        }
    }
}
