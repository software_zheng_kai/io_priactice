﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp10
{
    class Program
    {
        static void Main(string[] args)
        {
            /*1KB=1024B，1MB=1024KB，1GB=1024MB。
           DriveInfo driveInfo = new DriveInfo("C");
           Console.WriteLine("驱动器上的可用空闲空间量="+driveInfo.AvailableFreeSpace);
           Console.WriteLine("文件系统格式的名称="+driveInfo.DriveFormat);
           Console.WriteLine("获取驱动器的类型="+driveInfo.DriveType);
           Console.WriteLine("获取一个指示驱动器是否已准备好的值="+driveInfo.IsReady);
           Console.WriteLine("获取驱动器的名称="+driveInfo.Name);
           Console.WriteLine("获取驱动器的根目录="+driveInfo.RootDirectory);
           Console.WriteLine("获取驱动器上的可用空闲空间总量 (以字节为单位)="+driveInfo.TotalFreeSpace);
           Console.WriteLine("获取驱动器上存储空间的总大小="+driveInfo.TotalSize);



           Console.WriteLine();
           Console.WriteLine();
           Console.WriteLine();

           //获取计算机中所有驱动器的名称和文件格式。
           DriveInfo[] driveInfo = DriveInfo.GetDrives();
           foreach(DriveInfo v in driveInfo)
           {
               if (v.IsReady)
               {
                   Console.WriteLine();
                   Console.WriteLine("获取驱动器的名称=" + v.Name);
                   Console.WriteLine("驱动器上的可用空闲空间量=" + v.AvailableFreeSpace);
                   Console.WriteLine("文件系统格式的名称=" + v.DriveFormat);
                   Console.WriteLine("获取驱动器的类型=" + v.DriveType);
                   Console.WriteLine("获取驱动器的根目录=" + v.RootDirectory);
                   Console.WriteLine("获取驱动器上的可用空闲空间总量 (以字节为单位)=" + v.TotalFreeSpace);
                   Console.WriteLine("获取驱动器上存储空间的总大小=" + v.TotalSize);
               }
               */

            //在C盘下创建一个文件夹，并且在文件夹里面在创建两个文件夹
            DirectoryInfo directoryInfo = new DirectoryInfo("D:\\生活");
            directoryInfo.Create();
            directoryInfo.CreateSubdirectory("赚钱");
            directoryInfo.CreateSubdirectory("养家");
            IEnumerable<DirectoryInfo> dir = directoryInfo.EnumerateDirectories();
            foreach (var b in dir)
            {
                Console.WriteLine("c盘生活文件夹里面的文件夹为=" + b.Name);
            }

            //使用 Delete 方法即可完成文件删除的操作
            //DirectoryInfo directoryInfo = new DirectoryInfo("D:\\生活");
            //directoryInfo.Delete(true);




            //C# Directory类：文件夹操作
            /*
            bool flag = Directory.Exists("D:\\生活");
            if (flag)
            {
            //如果有就删除该文件夹
                Directory.Delete("D:\\生活",true);
            }
            else
            {
            /没有就创建该文件夹
                Directory.CreateDirectory("D:\\生活");
            }
            */

            //C# FileInfo类：文件操作
            /*  Directory 只读属性，获取父目录的实例
              DirectoryName   只读属性，获取表示目录的完整路径的字符串
              Exists  只读属性，获取指定的文件是否存在，若存在返回 True，否则返回 False
              IsReadOnly 属性，获取或设置指定的文件是否为只读的
              Length  只读属性，获取文件的大小
              Name    只读属性，获取文件的名称
              Filelnfo CopyTo(string destFileName)    将现有文件复制到新文件，不允许覆盖现有文件
              Filelnfo CopyTo(string destFileName, bool overwrite)    将现有文件复制到新文件，允许覆盖现有文件
              FileStream Create() 创建文件
              void Delete()   删除文件
              void MoveTo(string destFileName)    将指定文件移到新位置，提供要指定新文件名的选项
              Filelnfo Replace(string destinationFileName, string destinationBackupFileName)  使用当前文件对象替换指定文件的内容，先删除原始文件， 再创建被替换文件的备份
              */
            //在D盘下创建code文件夹
            Directory.CreateDirectory("D:\\code");
            FileInfo fileInfo = new FileInfo("D:\\code\\test1.txt");
            if (!fileInfo.Exists)
            {
                //创建文件
                fileInfo.Create().Close();
            }
            fileInfo.Attributes = FileAttributes.Normal;//设置文件属性
            Console.WriteLine("文件路径：" + fileInfo.Directory);
            Console.WriteLine("文件名称：" + fileInfo.Name);
            Console.WriteLine("文件是否只读：" + fileInfo.IsReadOnly);
            Console.WriteLine("文件大小：" + fileInfo.Length);
            //先创建code-1 文件夹
            //将文件移动到code-1文件夹下
            Directory.CreateDirectory("D:\\code-1");
            //判断目标文件夹中是否含有文件test1.txt
            FileInfo newFileInfo = new FileInfo("D:\\code-1\\test1.txt");
            if (!newFileInfo.Exists)
            {
                //移动文件到指定路径
                fileInfo.MoveTo("D:\\code-1\\test1.txt");
            }


            //C# File类：文件操作
            /*
            DateTime GetCreationTime(string path)   返回指定文件或目录的创建日期和时间
            DateTime GetLastAccessTime(string path)     返回上次访问指定文件或目录的日期和时间
            DateTime GetLastWriteTime(string path)  返回上次写入指定文件或目录的日期和时间
            void SetCreationTime(string path, DateTime creationTime)    设置创建该文件的日期和时间
            void SetLastAccessTime(string path, DateTime lastAccessTime)    设置上次访问指定文件的日期和时间
            void SetLastWriteTime(string path, DateTime lastWriteTime)  设置上次写入指定文件的日期和时间
            File 类是静态类，所提供的类成员也是静态的，调用其类成员直接使用 File 类的名称调用即可。
            */

            //在D盘下创建code文件夹
            Directory.CreateDirectory("D:\\code");
            Directory.CreateDirectory("D:\\code-1");
            string path = "D:\\code\\test1.txt";
            //创建文件
            FileStream fs = File.Create(path);
            //获取文件信息
            Console.WriteLine("文件创建时间：" + File.GetCreationTime(path));
            Console.WriteLine("文件最后被写入时间：" + File.GetLastWriteTime(path));
            //关闭文件流
            fs.Close();
            //设置目标路径
            string newPath = "D:\\code-1\\test1.txt";
            //判断目标文件是否存在
            bool flag = File.Exists(newPath);
            if (flag)
            {
                //删除文件
                File.Delete(newPath);
            }
            File.Move(path, newPath);




            //C# Path类：文件路径操作
            /*
            string ChangeExtension(string path, string extension)   更改路径字符串的扩展名
string Combine(params string[] paths)   将字符串数组组合成一个路径
string Combine(string path1, string path2)  将两个字符串组合成一个路径
string GetDirectoryName(string path)    返回指定路径字符串的目录信息
string GetExtension(string path)    返回指定路径字符串的扩展名
string GetFileName(string path) 返回指定路径字符串的文件名和扩展名
string GetFileNameWithoutExtension(string path) 返回不具有扩展名的指定路径字符串的文件名
string GetFullPath(string path) 返回指定路径字符串的绝对路径
char[] GetInvalidFileNameChars()    获取包含不允许在文件名中使用的字符的数组
char[] GetInvalidPathChars()    获取包含不允许在路径名中使用的字符的数组
string GetPathRoot(string path) 获取指定路径的根目录信息
string GetRandomFileName()  返回随机文件夹名或文件名
string GetTempPath()    返回当前用户的临时文件夹的路径
bool HasExtension(string path)  返回路径是否包含文件的扩展名
bool IsPathRooted(string path)  返回路径字符串是否包含根
*/


           /* Console.WriteLine("请输入一个文件路径：");
            string path = Console.ReadLine();
            Console.WriteLine("不包含扩展名的文件名：" + Path.GetFileNameWithoutExtension(path));
            Console.WriteLine("文件扩展名：" + Path.GetExtension(path));
            Console.WriteLine("文件全名：" + Path.GetFileName(path));
            Console.WriteLine("文件路径：" + Path.GetDirectoryName(path));
           */
            //更改文件扩展名
            string newPath = Path.ChangeExtension(path, "doc");
            Console.WriteLine("更改后的文件全名：" + Path.GetFileName(newPath));
           

        }
    }
}
