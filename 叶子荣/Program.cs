﻿using System;
using System.IO;
using System.Net.Http.Headers;

namespace IO
{
    class Program
    {
        static void Main(string[] args)
        {
            DriveInfo driveInfo = new DriveInfo("C");
            Console.WriteLine("驱动器的名称是：", driveInfo.Name);
            Console.WriteLine("驱动器的类型是：", driveInfo.DriveType);
            Console.WriteLine("驱动器的文件格式是：", driveInfo.DriveFormat);
            Console.WriteLine("驱动器的可用空间大小：", driveInfo.TotalFreeSpace);
            Console.WriteLine("驱动器总大小：", driveInfo.TotalSize);

            DriveInfo[] driveInfo1 = DriveInfo.GetDrives();
            foreach (var item in driveInfo1)
            {
                if (item.IsReady)
                {
                    Console.WriteLine("驱动器名称", item.Name);
                    Console.WriteLine("驱动器的文件格式", item.DriveFormat);
                }
            }


            //在D盘下创建code文件夹
            Directory.CreateDirectory("D:\\code");
            FileInfo fileInfo = new FileInfo("D:\\code\\test1.txt");
            if (!fileInfo.Exists)
            {
                //创建文件
                fileInfo.Create().Close();
            }
            fileInfo.Attributes = FileAttributes.Normal;//设置文件属性
            Console.WriteLine("文件路径：" + fileInfo.Directory);
            Console.WriteLine("文件名称：" + fileInfo.Name);
            Console.WriteLine("文件是否只读：" + fileInfo.IsReadOnly);
            Console.WriteLine("文件大小：" + fileInfo.Length);
            //先创建code-1 文件夹
            //将文件移动到code-1文件夹下
            Directory.CreateDirectory("D:\\code-1");
            //判断目标文件夹中是否含有文件test1.txt
            FileInfo newFileInfo = new FileInfo("D:\\code-1\\test1.txt");
            if (!newFileInfo.Exists)
            {
                //移动文件到指定路径
                fileInfo.MoveTo("D:\\code-1\\test1.txt");
            }

            //在D盘下创建code文件夹
            Directory.CreateDirectory("D:\\code");
            Directory.CreateDirectory("D:\\code-1");
            string path = "D:\\code\\test1.txt";
            //创建文件
            FileStream fs = File.Create(path);
            //获取文件信息
            Console.WriteLine("文件创建时间：" + File.GetCreationTime(path));
            Console.WriteLine("文件最后被写入时间：" + File.GetLastWriteTime(path));
            //关闭文件流
            fs.Close();
            //设置目标路径
            string newPath = "D:\\code-1\\test1.txt";
            //判断目标文件是否存在
            bool flag = File.Exists(newPath);
            if (flag)
            {
                //删除文件
                File.Delete(newPath);
            }
            File.Move(path, newPath);



            string str = @"D:\ChangZhi\dnplayer2\data\LoL.com";
            Console.WriteLine(Path.GetFileName(str));           //GetFileName  获取文件名字
            Console.WriteLine(Path.GetFileNameWithoutExtension(str));        //GetFileWithouExtension  获取文件名字但不包括扩展名
            Console.WriteLine(Path.GetExtension(str));          //获取扩展名
            Console.WriteLine(Path.GetDirectoryName(str));      //获取这个文件所在的文件夹名字
            Console.WriteLine(Path.GetFullPath(str));           //获取文件的绝对路径
            Console.WriteLine(Path.Combine(@"c:\a\", @"b.txt"));     // 连接两个字符串作为路径
            //int index = str.LastIndexOf("\\");
            //str = str.Substring(index + 1);
            Console.WriteLine(str);



            //创建一个文件
            //File.Create(@"C:\Users\叶子荣\Desktop\new.txt");
            //Console.WriteLine("创建成功");

            //删除一个文件
            //File.Delete(@"C:\Users\叶子荣\Desktop\new.txt");
            //Console.WriteLine("删除成功");

            //复制一个文件
            //File.Copy(@"C:\Users\叶子荣\Desktop\1.txt", @"C:\Users\叶子荣\Desktop\new.txt");
            //Console.WriteLine("复制成功");






        }
    }
}
