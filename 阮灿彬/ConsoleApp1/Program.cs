﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            DriveInfo deiveInfo = new DriveInfo("D");
            Console.WriteLine("驱动器的名称" + deiveInfo.Name);
            Console.WriteLine("驱动器可用空闲空间总量" + deiveInfo.TotalFreeSpace);
            Console.WriteLine("驱动器类型" + deiveInfo.DriveType);
            Console.WriteLine("驱动器空间总大小" + deiveInfo.TotalSize);
            Console.WriteLine("驱动器文件格式" + deiveInfo.DriveFormat);

            Console.WriteLine();

            DirectoryInfo directoryInfo = new DirectoryInfo("D:\\test");
            directoryInfo.Create();
            directoryInfo.CreateSubdirectory("te");
            IEnumerable<DirectoryInfo> dir = directoryInfo.EnumerateDirectories();
            foreach (var v in dir)
            {
                Console.WriteLine(v.Name);
            }
            //directoryInfo.Delete(true);
            //bool flag = Directory.Exists("D:\\test");
            //if (flag)
            //{
            //    Directory.Delete("D:\\test", true);
            //}
            // else
            // {
            //    Directory.CreateDirectory("D:\\test");
            //}

            FileInfo fileInfo = new FileInfo("D:\\test\\te.txt");
            if (!fileInfo.Exists)
            {
                
                fileInfo.Create().Close();
            }
            fileInfo.Attributes = FileAttributes.Normal;
            Directory.CreateDirectory("D:\\test");
            FileInfo newFileInfo = new FileInfo("D:\\test\\te.txt");
            if (!newFileInfo.Exists)
            {
                fileInfo.MoveTo("D:\\te\\te.txt");
            }
        }
    }
}
