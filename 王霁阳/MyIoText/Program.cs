﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MyIoText
{
    class Program
    {
        static void Main(string[] args)
        {
            //1、在D盘根目录下创建一个叫：mySolo的文件夹（注意，创建前可能需要判断该文件夹是否存在）；


            //判断有没有mySolo的文件夹存在
            bool a = Directory.Exists("D:\\mySolo");
            if (a)
            {
                Console.WriteLine("对不起，D盘下已经有了该名字的文件夹,不过我已经帮你删除了");
                Directory.Delete("D:\\mySolo", true);
            }
                //创建文件夹
                Directory.CreateDirectory("D:\\mySolo");

            //2、在上一步创建的目录中，创建一个叫test.txt的文件；
            FileInfo fileInfo = new FileInfo("D:\\mySolo\\text.txt");
            fileInfo.Create().Close();
            //3、在第2步创建的文件中，写内容，比如最喜欢的几首歌等； （注意，以上任务，可不限定于Driveinfo 类、Directory 类、Directoryinfo 类、File 类、Filelnfo 类、Path 类等，可自由选择和组合）
            //定义路径
            string text = @"D:\\mySolo\\text.txt";
            StreamWriter stream = new StreamWriter(text);
            stream.WriteLine("终于做好了");
            //刷新缓存
            stream.Flush();
            //关闭流
            stream.Close();




        }
    }
}
