﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.实例方法:
            //添加文件夹
            DirectoryInfo info1 = new DirectoryInfo("D:/mySolo");
            if (info1.Exists)
            {
                info1.Delete();

            }
            info1.Create();
            ////添加文件
            FileInfo info2 = new FileInfo("D:/mySolo/test.txt");
            if (info2.Exists)
            {
                info2.Delete();
            }
            info2.Create();
            
            //使用FileStream来读取数据
            FileStream fsRead = new FileStream(@"D:\mySolo\test.txt",FileMode.Open);
            byte[] capacity = new byte[1024 * 1024 * 5];
            int reality = fsRead.Read(capacity, 0, capacity.Length);
            string s = Encoding.UTF8.GetString(capacity, 0, reality);

            fsRead.Close();
            fsRead.Dispose();
            Console.WriteLine(s);

            //使用FileStream来写入文件
            using (FileStream fsWrite = new FileStream(@"D:\mySolo\test.txt", FileMode.OpenOrCreate, FileAccess.Write))
            {
                string str = "温柔  倔强  知足  时光机  志明与春娇 ";
                byte[] buffer = Encoding.UTF8.GetBytes(str);
                fsWrite.Write(buffer, 0, buffer.Length);
            }
            Console.WriteLine("写入成功");

            //使用StreamWriter来写入文件
            using(StreamWriter sw = new StreamWriter(@"D:\mySolo\test.txt", true))
            {
                sw.Write("入海  无问  一荤一素 一程山路");
            }
            Console.WriteLine("再次写入成功");



            //2.静态方法:
            //添加文件夹
            bool flag1 = Directory.Exists("D:/mySolo");
            if (flag1)
            {
                Directory.Delete("D:/mySolo");
            }
            else
            {
                Directory.CreateDirectory("D:/mySolo");
            }

            //添加文件
            bool flag2 = File.Exists("D:/mySolo/test.txt");
            if (flag2)
            {
                File.Delete("D:/mySolo/test.txt");
            }
            else
            {
                File.Create("D:/mySolo/test.txt");
            }

        }
    }
}

        
