﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            DriveInfo driveInfo = new DriveInfo("D");
            Console.WriteLine("驱动器类型:" + driveInfo.DriveType);
            Console.WriteLine("驱动器名称:" + driveInfo.Name);
            Console.WriteLine("驱动器空闲空间总量:" + driveInfo.TotalFreeSpace);
            Console.WriteLine("驱动器的总大小:" + driveInfo.TotalSize);
            Console.WriteLine("文件系统格式名称:" + driveInfo.DriveFormat);
            //在D盘创建文件夹
            DirectoryInfo directoryInfo = new DirectoryInfo("D:\\test");
            //创建目录
            directoryInfo.Create();
            //创建子文件夹
            directoryInfo.CreateSubdirectory("test-1");
            directoryInfo.CreateSubdirectory("test-2");
            //返回当前目录中目录信息的可枚举集合
            IEnumerable<DirectoryInfo> directories = directoryInfo.EnumerateDirectories();
            //查看test文件夹里的文件夹
            foreach (var a in directories)
            {
                Console.WriteLine(a.Name);
            }
            //删除test文件夹及其子文件夹
            directoryInfo.Delete(true);
            //在 D 盘上操作 code 文件夹，先判断是否存在该文件夹，如果存在则删除，否则创建该文件夹。
            bool b = Directory.Exists("D:\\code");
            if (b)
            {
                Directory.Delete("D:\\code", true);
            }
            else
            {
                Directory.CreateDirectory("D:\\code");
            }
            //在D盘创建文件夹Demo
            Directory.CreateDirectory("D:\\Demo");
            //在Demo里创建文件demo01
            FileInfo fileInfo = new FileInfo("D:\\Demo\\demo01.txt");
            if (!fileInfo.Exists)
            {
                fileInfo.Create().Close();
            }
            fileInfo.Attributes = FileAttributes.Normal;//设置文件属性
            Console.WriteLine("文件路径：" + fileInfo.Directory);
            Console.WriteLine("文件名称：" + fileInfo.Name);
            Console.WriteLine("文件是否只读：" + fileInfo.IsReadOnly);
            Console.WriteLine("文件大小：" + fileInfo.Length);
            //创建文件夹Demo-01
            Directory.CreateDirectory("D:\\Demo-01");
            //判断目标文件夹是否含有文件,如果没有将文件移动到Demo-01文件夹里
            FileInfo file = new FileInfo("D:\\Demo-01\\demo01.txt");
            if (!file.Exists)
            {
                fileInfo.MoveTo("D:\\Demo-01\\demo01.txt");
            }
            string s = "D:\\Demo\\demo01.txt";
            FileStream fileStream = File.Create(s);
            Console.WriteLine("文件创建时间:" + File.GetCreationTime(s));
            Console.WriteLine("文件最后被写入时间:" + File.GetLastWriteTime(s));
            //关闭文件流
            fileStream.Close();
            //设置文件路径
            string newS = "D:\\Demo-01\\demo01.txt";
            //判断目标文件是否存在
            bool c = File.Exists(newS);
            if (c)
            {
                //删除文件
                File.Delete(newS);
            }
            File.Move(s, newS);
            //Path类
            Console.WriteLine("输入一个文件路径:");
            string path = Console.ReadLine();
            Console.WriteLine("不包含扩展名的文件名:" + Path.GetFileNameWithoutExtension(path));
            Console.WriteLine("文件扩展名:" + Path.GetExtension(path));
            Console.WriteLine("文件全名:" + Path.GetFileName(path));
            Console.WriteLine("文件路径:" + Path.GetDirectoryName(path));
            //更改文件扩展名
            string newPath = Path.ChangeExtension(path, "ado");
            Console.WriteLine("更改后文件全名:" + Path.GetFileName(newPath));
        }
    }
}
