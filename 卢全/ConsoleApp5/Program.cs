﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Driveinfo：获取计算机所有驱动器信息
            DriveInfo[] driveInfo = DriveInfo.GetDrives();//检索计算机上所有驱动器名称
            foreach (DriveInfo p in driveInfo)
            {
                if (p.IsReady)//指示驱动器是否已准备好
                {
                    Console.WriteLine($"驱动器名称：{p.Name}");//获取驱动器名称
                    Console.WriteLine($"驱动器可用空间量：{p.AvailableFreeSpace}");//获取驱动器可用空间量（字节为单位）
                    Console.WriteLine($"驱动器文件格式：{p.DriveFormat}");//获取驱动器格式名称
                    Console.WriteLine($"驱动器类型：{p.DriveType}");//获取驱动器类型            
                    Console.WriteLine($"驱动器总大小：{p.TotalSize}");//获取驱动器空间总量（字节为单位）
                    Console.WriteLine();
                }
            }

            //Directoryinfo类：文件夹操作
            //在F盘创建文件夹text
            DirectoryInfo directoryInfo = new DirectoryInfo("F:\\text");
            directoryInfo.Create();
            Console.WriteLine("文件夹创建成功！");
            //在text文件夹内创建文件夹s-1和f-2
            directoryInfo.CreateSubdirectory("s-1");
            Console.WriteLine("文件夹创建成功！");
            directoryInfo.CreateSubdirectory("f-2");
            Console.WriteLine("文件夹创建成功！");
            //查看是否在text文件夹内创建文件夹
            IEnumerable<DirectoryInfo> dir = directoryInfo.EnumerateDirectories();
            foreach (var v in dir)
            {
                Console.WriteLine(v.Name);
            }
            //删除文件夹text内的f-2文件
            DirectoryInfo de = new DirectoryInfo("F:\\text\\f-2");
            de.Delete(true);
            Console.WriteLine("文件夹删除成功！");
            //查看是否删除文件夹text内的f-2文件
            foreach (var v in dir)
            {
                Console.WriteLine(v.Name);
            }
            //Directory类：文件夹操作
            //在F盘创建文件夹text先判断文件夹是否存在
            if (Directory.Exists("F:\\text"))
            {
                Console.WriteLine("文件夹已存在！");
                Directory.Delete("F:\\text", true);//存在就删除
                Console.WriteLine("文件夹已删除！");
            }
            else
            {
                Console.WriteLine("文件夹不存在！");
                Directory.CreateDirectory("F:\\text");//不存在直接创建
                Console.WriteLine("文件夹创建成功！");
            }

            //FileInfo类：文件操作
            //在F盘下创建text文件夹
            Directory.CreateDirectory("F:\\text");
            FileInfo fileInfo = new FileInfo("F:\\text\\test1.txt");
            if (!fileInfo.Exists)
            {
                //创建文件
                fileInfo.Create().Close();
            }
            fileInfo.Attributes = FileAttributes.Normal;//设置文件属性
            //查看文件信息
            Console.WriteLine("文件路径：" + fileInfo.Directory);
            Console.WriteLine("文件名称：" + fileInfo.Name);
            Console.WriteLine("文件是否只读：" + fileInfo.IsReadOnly);
            Console.WriteLine("文件大小：" + fileInfo.Length);
            //先创建s-1 文件夹
            //将文件移动到s-1文件夹下
            Directory.CreateDirectory("F:\\s-1");
            //判断目标文件夹中是否含有文件test1.txt
            FileInfo newFileInfo = new FileInfo("F:\\s-1\\test1.txt");
            if (!newFileInfo.Exists)
            {
                //移动文件到指定路径
                fileInfo.MoveTo("F:\\s-1\\test1.txt");
            }

            // File类：文件操作
            //获取文件信息
            Console.WriteLine("文件创建时间：" + File.GetCreationTime("F:\\s-1\\test1.txt"));
            Console.WriteLine("文件最后被写入时间：" + File.GetLastWriteTime("F:\\s-1\\test1.txt"));
            //判断目标文件是否存在
            if (File.Exists("F:\\s-1\\test1.txt"))
            {
                //删除文件
                File.Delete("F:\\s-1\\test1.txt");
                Console.WriteLine("删除成功");
            }

            //Path类：文件路径操作

            string path = "F://s-1//111.text";
            Console.WriteLine("不包含扩展名的文件名：" + Path.GetFileNameWithoutExtension(path));
            Console.WriteLine("文件扩展名：" + Path.GetExtension(path));
            Console.WriteLine("文件全名：" + Path.GetFileName(path));
            Console.WriteLine("文件路径：" + Path.GetDirectoryName(path));
            //更改文件扩展名
            string newPath = Path.ChangeExtension(path, "doc");
            Console.WriteLine("更改后的文件全名：" + Path.GetFileName(newPath));
        }
    }
}
